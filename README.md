# PuRAS



## Getting started

1. Fork this repository.
2. Finish the course: https://courses.cd.training/courses/tdd-tutorial
3. Select one kata from: https://cyber-dojo.org/creator/home
4. Add you name in [the list](kata/kata_assignees.md) (one person per kata!). You can use https://typora.io/ for editing markdown files.
5. Add test_kata.py to .gitlab-ci.yml
6. Follow Red->Green->Refactor cycle during Kata development and commit after each green and refactor stage. Do not commit Red.

Have fun!!!
