"""Test modul for GrayCode class"""

import unittest

from graycode import GrayCode


class TestEncoder(unittest.TestCase):
    """Test class for Encoder method"""

    def setUp(self):
        self.graycode = GrayCode()
    
    def test_should_confirm_that_encoder_encodes_1b_numbers_correctly(self):
        """Testing Scenario_1, 1b numbers are successfully decoded"""
        self.assertEqual(self.graycode.encode(1), 1)

    def test_should_confirm_that_encoder_encodes_2b_numbers_correctly(self):
        """Testing Scenario_1, 2b numbers  are successfully decoded"""
        self.assertEqual(self.graycode.encode(3), 2)

    def test_should_confirm_that_encoder_encodes_3b_numbers_correctly(self):
        """Testing Scenario_1, 3b numbers  are successfully decoded"""
        self.assertEqual(self.graycode.encode(5), 7)

    def test_should_confirm_that_encoder_encodes_5b_numbers_correctly(self):
        """Testing Scenario_1, 5b numbers  are successfully decoded"""
        self.assertEqual(self.graycode.encode(31), 16)

class TestDecoder(unittest.TestCase):
    """Test class for Decoder method"""

    def setUp(self):
        self.graycode = GrayCode()
    
    def test_should_confirm_that_decoder_decodes_1b_numbers_correctly(self):
        """Testing Scenario_2, 1b numbers are successfully decoded"""
        self.assertEqual(self.graycode.decode(1), 1)

    def test_should_confirm_that_decoder_decodes_2b_numbers_correctly(self):
        """Testing Scenario_2, 2b numbers are successfully decoded"""
        self.assertEqual(self.graycode.decode(2), 3)

    def test_should_confirm_that_decoder_decodes_3b_numbers_correctly(self):
        """Testing Scenario_2, 3b numbers are successfully decoded"""
        self.assertEqual(self.graycode.decode(7), 5)

    def test_should_confirm_that_decoder_decodes_5b_numbers_correctly(self):
        """Testing Scenario_2, 5b numbers are successfully decoded"""
        self.assertEqual(self.graycode.decode(16), 31)

class TestDisplay(unittest.TestCase):
    """Test class for Display method"""

    def setUp(self):
        self.graycode = GrayCode()

    def test_should_confirm_that_function_displays_numbers_in_binary_form(self):
        """Testing Scenario_3, number successfully displayed"""
        self.assertEqual(self.graycode.display(31), "0b11111")

unittest.main()
