"""GrayCode modul"""


class GrayCode:
    """GrayCode class"""

    def encode(self, number):
        g = 5 * [0]

        g[0] = (number >> 4) & 1

        for i in range (1, 5):
            current = (number >> (4-i) & 1)
            before = (number >> (5-i) & 1)

            if current == 1:
                g[i] =  before ^ 1
            else:
                g[i] = before
        
        return int(''.join(map(str, g)), 2) 
   
    def decode(self, number): 
        b = 5 * [0]
        b[0] = (number >> 4) & 1

        for i in range (1, 5):
            b[i] = ((number >> (4-i) & 1)) ^ b[i-1] 
            
        return int(''.join(map(str, b)), 2)

    def display(self, number):
        ret = bin(number)
        return ret

