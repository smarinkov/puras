# Feature: GrayCode

## Narrative:

**As a** customer <br>
**I want** to encode, decode, and display all 5-bit binary numbers using Gray code<br>
**So that** I could facilitate error correction in digital communications <br>

## Scenario_1: Numbers are successfully encoded

**Given**: Any 5-bit number is entered <br>
**When**: The 5-bit number is entered <br>
**Then**: The encoder should return encoded 5-bit number <br>

## Scenario_2: Numbers are successfully decoded

**Given**: Any 5-bit number is entered <br>
**When**: The encoded 5-bit number is entered <br>
**Then**: The decoder should return decoded 5-bit number <br>

## Scenario_3: Display decimal number as binary

**Given**: Any 5-bit number is entered <br>
**When**: The 5-bit number is entered <br>
**Then**: Display its binary representation <br>